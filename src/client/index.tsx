import { hydrate } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { RecoilRoot } from 'recoil';

import { App } from 'src/client/App';
import { Head } from 'src/client/Head';

hydrate(
    <BrowserRouter>
        <RecoilRoot>
            <Head />
            <App />
        </RecoilRoot>
    </BrowserRouter>,
    document.getElementById('app'),
);
