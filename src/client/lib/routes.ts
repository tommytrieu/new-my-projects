export const HOME_PATH = '/home';
export const ERROR_PATH = '/error';
export const NOT_FOUND = '/404';
export const UNAUTHORIZED = '/403';
export const DEFAULT = '/';
