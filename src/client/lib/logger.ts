import { 
    Logger,
    consoleTransporter,
    LoggerConfig,
    ILogger,
    Transport,
} from '@vp/js-logger';

const config = {
    level: 'debug',
    threaded: false,
};
const transports: Transport[] = [consoleTransporter];

const logger = new Logger({
    ...config,
    transports,
} as LoggerConfig);

export function getLogger(): ILogger {
    return logger;
}
