/* eslint-disable no-underscore-dangle */
export function config(): App.Config {
    // @ts-ignore
    // make it a hook because you CAN use hooks in the selector
    return typeof (window) !== 'undefined' && window.__APP_CONFIG__ as App.Config;
}
