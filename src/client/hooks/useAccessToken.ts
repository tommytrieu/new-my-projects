import VistaprintAuth from '@vp/auth';
import { useEffect, useState } from 'react';
import { useRecoilValue } from 'recoil';

import { localeState } from 'src/client/atoms/locale';
import { config } from 'src/client/lib/config';

export const useAccessToken = (): string | false | undefined => {
    const [accessToken, setAccessToken] = useState();
    const culture = useRecoilValue(localeState);

    useEffect(() => {
        const configuration: App.Config = config();

        const login = async (): Promise<void> => {
            debugger;

            await VistaprintAuth.init({
                culture,
                developmentMode: configuration?.auth?.developmentMode
                    ? {
                        clientID: configuration.auth.clientId,
                        redirectUriOrigin: `${window.location.origin}`,
                    }
                    : false,
            });

            const authInstance = new VistaprintAuth.WebAuth();

            if (!authInstance.isSignedIn()) {
                authInstance.signIn();
            } else {
                const token = authInstance.getToken();

                setAccessToken(!!token && token);
            }
        };

        login();
    }, []);

    return accessToken;
};
