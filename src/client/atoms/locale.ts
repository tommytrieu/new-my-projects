import { getCurrentCulture } from '@vp/culture-resolution';
import { atom } from 'recoil';

export const localeState = atom({
    key: 'locale',
    default: 'en-ie',
    effects_UNSTABLE: [
        ({ setSelf }): void => {
            setSelf(getCurrentCulture());
        },
    ],
});
