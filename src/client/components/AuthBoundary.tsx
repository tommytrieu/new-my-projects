import { HTMLProps } from 'react';

import { UnauthorizedPage } from 'src/client/pages/UnauthorizedPage';
import { useAccessToken } from 'src/client/hooks/useAccessToken';
import { Typography } from '@material-ui/core';

export const AuthBoundary = (props: HTMLProps<unknown>): JSX.Element => {
    const { children } = props;
    const accessToken = useAccessToken();

    return (
        <>
            {/* TODO better loading state / splash screen */}
            {accessToken === undefined && (<Typography>Loading...</Typography>)}
            {accessToken === false && (<UnauthorizedPage />)}
            {accessToken && children}
        </>
    );
};
