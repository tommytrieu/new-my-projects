import {
    Switch,
    Route,
    Redirect,
} from 'react-router-dom';

import { ErrorPage } from 'src/client/pages/ErrorPage';
import { HomePage } from 'src/client/pages/HomePage';
import { NotFoundPage } from 'src/client/pages/NotFoundPage';
import { UnauthorizedPage } from 'src/client/pages/UnauthorizedPage';
import {
    DEFAULT,
    ERROR_PATH,
    HOME_PATH,
    NOT_FOUND,
    UNAUTHORIZED,
} from 'src/client/lib/routes';

export const Routes = (): JSX.Element => (
    <Switch>
        {/* TODO pass qs parameters as Error to component */}
        <Route path={ERROR_PATH} component={ErrorPage} />
        <Route path={NOT_FOUND} component={NotFoundPage} />
        <Route path={UNAUTHORIZED} component={UnauthorizedPage} />
        <Route path={HOME_PATH} component={HomePage} />
        <Redirect from={DEFAULT} to={NOT_FOUND} />
    </Switch>
);
