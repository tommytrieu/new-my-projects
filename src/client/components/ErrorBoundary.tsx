import { Component, ErrorInfo } from 'react';

import { ErrorPage } from 'src/client/pages/ErrorPage';
import { getLogger } from 'src/client/lib/logger';
import { AppError } from 'src/shared/errors';

interface PropTypes {
    children: React.ReactNode;
}

interface State {
    error: Error | AppError | null;
}

/**
 * Our app relies on handling errors at a central location to ensure consistent
 * reporting and consumption of any error-based states.
 *
 * Any error-based state, such as a non-recoverable network error, validation
 * error, etc., should be thrown from a Component to be caught and handled
 * in the ErrorBoundary.
 */
export class ErrorBoundary extends Component<PropTypes, State> {
    state: State = {
        error: null,
    };

    static getDerivedStateFromError(error: Error | AppError): State {
        return { error };
    }

    componentDidCatch(thrownError: Error, info: ErrorInfo): void {
        const error = new AppError(thrownError);

        getLogger().error({ message: error.message, info }, error);
    }

    render(): React.ReactNode {
        const { children } = this.props;
        const { error } = this.state;

        return error ? <ErrorPage error={error!} /> : children;
    }
}
