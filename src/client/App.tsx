import { hot } from 'react-hot-loader/root';
import { ThemeProvider } from '@emotion/react';
import { dttVisageInternalTheme } from '@vp/themes/packages/material-ui@v5';
import { CssBaseline } from '@material-ui/core';

import { ErrorBoundary } from 'src/client/components/ErrorBoundary';
import { AuthBoundary } from 'src/client/components/AuthBoundary';
import { Routes } from 'src/client/components/Routes';

export const App = hot((): JSX.Element => (
    <ThemeProvider theme={dttVisageInternalTheme}>
        <CssBaseline />
        <ErrorBoundary>
            <AuthBoundary>
                <Routes />
            </AuthBoundary>
        </ErrorBoundary>
    </ThemeProvider>
));
