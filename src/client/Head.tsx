import { Helmet } from 'react-helmet';

export const Head = (): JSX.Element => (
    <Helmet>
        <html lang="en" />
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="format-detection" content="telephone=no" />
        <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
        <meta name="theme-color" content="#006196" />
        <meta name="robots" content="noindex, nofollow" />
        {/* TODO noscript for disabled JS support */}
        <title>My Projects</title>
        <style>
            {`
                html, body, #app {
                    height: 100%;
                    width: 100%;
                }

                body {
                    margin: 0;
                }
            `}
        </style>
    </Helmet>
);
