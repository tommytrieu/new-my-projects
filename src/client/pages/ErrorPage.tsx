/** @jsx jsx */
import {
    Grid,
    makeStyles,
    Typography,
    Theme,
} from '@material-ui/core';

import { AppError } from 'src/shared/errors';

export interface PropTypes extends React.HTMLProps<HTMLDivElement> {
    error: (Error | AppError) & { guid?: string };
}

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        margin: 0,
        paddingTop: theme.spacing(4),
        width: '100%',
    },
    message: {
        whiteSpace: 'normal',
    },
}));

export const ErrorPage = (props: PropTypes): JSX.Element => {
    const classes = useStyles();
    const { error: { guid, message } } = props;

    return (
        <Grid
            container
            alignContent="center"
            className={classes.container}
            direction="column"
            spacing={10}
        >
            <Grid item xs>
                <Typography gutterBottom align="center" className="header" variant="h2">
                    Whoops!
                </Typography>
                <Typography gutterBottom align="center" className="subheader" variant="h3">
                    There&apos;s a slight problem on this page.
                </Typography>
            </Grid>
            <Grid item>
                {guid && (
                    <Typography
                        paragraph
                        align="center"
                        variant="body1"
                    >
                        {guid}
                    </Typography>
                )}
                <Typography
                    align="center"
                    className={classes.message}
                    component="pre"
                    variant="body1"
                >
                    {message}
                </Typography>
            </Grid>
        </Grid>
    );
};
