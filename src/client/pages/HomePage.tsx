import { Typography } from '@material-ui/core';

export const HomePage = (): JSX.Element => (
    <Typography variant="h1">Home sweet home</Typography>
);
