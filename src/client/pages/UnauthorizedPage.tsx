import { Theme } from '@material-ui/core';
import {
    Box,
    Button,
    Grid,
    Typography,
    makeStyles,
} from '@material-ui/core';
import { useRouteMatch } from 'react-router';

const useStyles = makeStyles((theme: Theme) => ({
    box: {
        paddingTop: theme.spacing(4),
    },
    logo: {
        display: 'inline',
        verticalAlign: 'middle',
    },
    page: {
        backgroundColor: theme.palette.primary.dark,
        color: theme.palette.common.white,
        height: '100%',
        textAlign: 'center',
    },
}));

const triggerReload = (): void => {
    window.location.reload();
};

export const UnauthorizedPage = (): JSX.Element => {
    const classes = useStyles();
    const match = useRouteMatch();

    return (
        <Grid
            container
            className={classes.page}
            justifyContent="center"
            alignItems="center"
            spacing={2}
        >
            <Grid item xs={4}>
                <Typography paragraph variant="h2">{match.path.replace('/', '')}</Typography>
                <Typography paragraph variant="h6">Unauthorized</Typography>
                <Box className={classes.box}>
                    <Button color="primary" variant="contained" onClick={triggerReload}>
                        Log in again
                    </Button>
                </Box>
            </Grid>
        </Grid>
    );
};
