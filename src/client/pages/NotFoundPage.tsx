/** @jsx jsx */
import { Link } from 'react-router-dom';
import {
    Box,
    Button,
    Grid,
    makeStyles,
    Typography,
    Theme,
} from '@material-ui/core';

import { HOME_PATH } from 'src/client/lib/routes';

const useStyles = makeStyles((theme: Theme) => ({
    box: {
        paddingTop: theme.spacing(4),
    },
    logo: {
        display: 'inline',
        verticalAlign: 'middle',
    },
    page: {
        backgroundColor: theme.palette.primary.dark,
        color: theme.palette.common.white,
        height: '100%',
        textAlign: 'center',
    },
}));

export const NotFoundPage = (): JSX.Element => {
    const classes = useStyles();

    return (
        <Grid
            container
            className={classes.page}
            xs={12}
            justifyContent="center"
            alignItems="center"
            spacing={1}
        >
            <Grid item xs={4}>
                <Typography paragraph variant="h2">404</Typography>
                <Typography paragraph variant="h6">Oops!</Typography>
                <Typography paragraph variant="h6">That page was not found</Typography>
                <Box className={classes.box}>
                    <Button
                        component={Link}
                        color="primary"
                        to={HOME_PATH}
                        variant="contained"
                    >
                        Home Page
                    </Button>
                </Box>
            </Grid>
        </Grid>
    );
};
