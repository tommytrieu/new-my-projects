import { v4 } from 'uuid';

export const AWS_DEFAULT_REGION = 'eu-west-1';
export const AWS_APP_CONFIG_CLIENT_ID = v4();
