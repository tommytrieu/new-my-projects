import 'source-map-support/register';
// import 'newrelic';

import { Server } from 'src/server/main';

const server = new Server();

server.start();
