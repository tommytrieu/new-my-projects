import * as AWS from 'aws-sdk';
import { GetConfigurationRequest } from 'aws-sdk/clients/appconfig';

AWS.config.update({ region: process.env.AWS_REGION || '' });

export const getConfiguration = async (): Promise<App.Config> => {
    const appConfig = new AWS.AppConfig({
        region: process.env.AWS_REGION,
    });
    const params: GetConfigurationRequest = {
        Application: process.env.APPLICATION || '',
        ClientId: process.env.CLIENT_ID || '',
        Configuration: process.env.CONFIGURATION || '',
        Environment: process.env.ENVIRONMENT || '',
    };

    const { Content } = await appConfig.getConfiguration(params).promise();

    if (!Content) {
        throw new Error('Could not retrieve configuration.');
    }

    const content = JSON.parse(Content.toString()) as App.Config;

    return Promise.resolve(content);
};
