import compress from 'koa-compress';
import helmet from 'koa-helmet';
import Koa from 'koa';
import { Server as NetServer } from 'net';

import { accessLogger } from 'src/server/middleware/accessLogger';
import { getLogger } from 'src/server/utils/logger';
import { killHandler } from 'src/server/utils/killHandler';
import { requestLogger } from 'src/server/middleware/requestLogger';
import { router } from 'src/server/router';
import { error } from './middleware/error';

const BACKLOG = 99;
const HOSTNAME = '0.0.0.0';
const LOGGER = getLogger();
const PORT = 80;

export class Server {
    app: Koa<any, {}>;

    server: NetServer | null = null;

    constructor() {
        this.app = new Koa();

        this.initializeHooks();
    }

    initializeHooks(): void {
        // pm2 graceful shutdown compatibility
        // Catches ctrl+c event
        process.on('SIGINT', killHandler(LOGGER, this.stop));

        // atexit handler
        process.on('exit', this.stop);

        // Centralized logging. Anytime the `error` event is called on the app
        // (i.e. when app.error is called), make sure that the
        // error is logged
        this.app.on('error', (err, ctx) => {
            LOGGER.error({
                url: ctx.request.url,
                message: 'Request cancelled: A fatal error occured',
            }, err);
        });
    }

    initializeMiddleware(): void {
        // Add common request security measures
        this.app.use(helmet({
            contentSecurityPolicy: false,
        }));
        this.app.use(requestLogger);
        this.app.use(accessLogger(LOGGER));
        this.app.use(compress());
        this.app.use(error);

        // Set server router
        this.app.use(router.routes());
        this.app.use(router.allowedMethods());
    }

    start(): void {
        try {
            this.initializeMiddleware();

            this.server = this.app.listen(
                PORT,
                HOSTNAME,
                BACKLOG,
                () => {
                    if (process.send) {
                        process.send('ready');
                    }
                    LOGGER.info(`Server listening at ${HOSTNAME}:${PORT}...`);
                },
            );
        } catch (e) {
            LOGGER.critical('There was an error starting the server', e);
        }
    }

    stop(): void {
        LOGGER.destroy();

        if (this && this.server) {
            this.server.close();
        }
    }
}
