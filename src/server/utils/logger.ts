import {
    consoleTransporter,
    Logger,
    LoggerConfig,
    LoggerContext,
    LogObject,
    TransformResult,
} from '@vp/js-logger';

const config: LoggerConfig = {
    level: 'debug',
    threaded: false,
};
const transports: Transport[] = [consoleTransporter];

const requestContextFactory = (data: object) => (
    message: LogObject,
    context: LoggerContext,
): TransformResult => {
    const modifiedContext = {
        ...context,
        ...data,
    };

    return {
        message,
        context: modifiedContext,
    };
}

export function createLogger(context: object): Logger {
    return new Logger({
        ...config,
        transports,
        transforms: [requestContextFactory(context)],
    });
}

export function getLogger(): Logger {
    return createLogger(config);
}
