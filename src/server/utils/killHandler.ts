/**
 * @module utils/killHandler
 * @exports killHandler
 */

/**
 * Handler for capturing a kill event and stopping the current process
 */
export function killHandler(logger: App.Logger.ILogger, cb: Function) {
    return () => {
        logger.info('Captured shutdown request');

        cb();

        process.exit(0);
    };
}
