import morgan from 'morgan';
import { ParameterizedContext } from 'koa';
import { IncomingMessage, ServerResponse } from 'http';

/**
 * Middleware that uses `moargan` to log all requests (access logging) using
 * the provided Logger instance. Our usage of `morgan` has been adapted for
 * our Koa middleware style.
 *
 * {@see Logger}
 * {@see https://github.com/expressjs/morgan}
 */
export const accessLogger = (logger: App.Logger.ILogger, format = 'combined', opts: object = {}) => {
    const morganLogger = morgan(format, {
        // Don't log access logs is response is OK or redirect
        skip: (req: IncomingMessage, res: ServerResponse) => res.statusCode < 400,
        stream: {
            write: logger.info.bind(logger),
        },
        ...opts,
    });

    return async (ctx: ParameterizedContext, next: Function) => {
        await new Promise((resolve, reject) => {
            // @ts-ignore
            morganLogger(ctx.req, ctx.res, (err: Error) => {
                err ? reject(err) : resolve(ctx);
            });
        });

        await next();
    };
};
