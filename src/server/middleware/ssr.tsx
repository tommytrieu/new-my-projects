import { ParameterizedContext } from 'koa';

import { View } from 'src/server/views';
import { getConfiguration } from 'src/server/lib/config';

export const ssr = async (ctx: ParameterizedContext): Promise<void> => {
    const config = await getConfiguration();

    ctx.type = 'html';
    ctx.body = new View(config, ctx);

    // const key = 'myprojects';
    // const cache = createCache({ key });
    // const { extractCritical } = createEmotionServer(cache);
    // const routerContext: StaticRouterContext = {};

    // const { html, css, ids } = extractCritical(renderToString(
    //     <StaticRouter location={ctx.req.url} context={routerContext}>
    //         <CacheProvider value={cache}>
    //             <RecoilRoot>
    //                 <Head />
    //                 <App />
    //             </RecoilRoot>
    //         </CacheProvider>
    //     </StaticRouter>,
    // ));
    // const helmet = Helmet.renderStatic();

    // if (routerContext.url) {
    //     return ctx.redirect(routerContext.url);
    // }

    // ctx.body = `
    //     <!doctype html>
    //     <html ${helmet.htmlAttributes.toString()}>
    //         <head>
    //             ${helmet.title.toString()}
    //             ${helmet.meta.toString()}
    //             ${helmet.link.toString()}
    //             <style data-emotion="${key} ${ids.join(' ')}">${css}</style>
    //         </head>
    //         <body ${helmet.bodyAttributes.toString()}>
    //             <script>
    //                 window.__APP_CONFIG__ = Object.freeze(${JSON.stringify(config)});
    //             </script>
    //             <div id="app">
    //                 ${html}
    //             </div>
    //             <script type="text/javascript" src="${PUBLIC_PATH}/index.js"></script>
    //         </body>
    //     </html>
    // `;
    // ctx.status = 200;

    // return undefined;

    // .pipe(through2(
    //     function write(chunk, enc, callback) {
    //         this.push(chunk);
    //     },
    //     function endPipe(this: any) {
    //         if (routerContext.url) {
    //             const html = renderToString(
    //                 <StaticRouter location={routerContext.url}>
    //                     <RecoilRoot>
    //                         <App />
    //                     </RecoilRoot>
    //                 </StaticRouter>,
    //             );

    //             this.queue(html);
    //         }

    //         this.queue(end(PUBLIC_PATH));
    //         this.queue(null);
    //     },
    // ))
    // .pipe(ctx.res);
};
