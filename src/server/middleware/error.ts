import { ParameterizedContext } from "koa";

import { NotFoundError } from "src/shared/errors/NotFoundError";
import { getLogger } from "src/server/utils/logger";
import { AppError } from "src/shared/errors";
import { ERROR_PATH } from "src/client/lib/routes";

const LOGGER = getLogger();

export const error = async (ctx: ParameterizedContext, next: Function) => {
    try {
        await next();

        // Intercept any 404 responses to handle them properly
        if (ctx.status === 404) {
            throw new NotFoundError({ message: 'Not Found', url: ctx.req.url || '' });
        }
    } catch (e) {
        LOGGER.error(e instanceof NotFoundError ? e : new AppError(e));
        ctx.status = 301;

        return ctx.redirect(ERROR_PATH);
    }
};
