import { ParameterizedContext } from 'koa';

import { createLogger } from 'src/server/utils/logger';

export const requestLogger = async (ctx: ParameterizedContext, next: Function) => {
    // Try to pull out the client IP from two sources, in precedence:
    // 1. Akamai's True-Client-IP
    // 2. Koa's IP implementation (use X-Forwarded-For, falling back to the socket's remoteAddr)
    const trueClientIpHeader = ctx.request.get('True-Client-IP');
    const clientIp = trueClientIpHeader || ctx.request.ip;

    ctx.logger = createLogger({
        clientIp,
        url: ctx.req.url,
    });

    try {
        await next();
    } finally {
        ctx.logger.destroy();
    }
};
