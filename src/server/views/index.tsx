/* eslint-disable no-underscore-dangle */
import { Helmet, HelmetData } from 'react-helmet';
import { Readable } from 'stream';
import { ParameterizedContext } from 'koa';
import { RecoilRoot } from 'recoil';
import { renderStylesToNodeStream } from '@emotion/server';
import { StaticRouter, StaticRouterContext } from 'react-router';
import { renderToNodeStream, renderToString } from 'react-dom/server';
import co from 'co';

import { App } from 'src/client/App';
import { Head } from 'src/client/Head';

declare const PUBLIC_PATH: string;

const end = `</div>
            <script type="text/javascript" src="${PUBLIC_PATH}index.js"></script>
        </body>
    </html>
`;

const start = (config: App.Config, helmet: HelmetData): string => (`
    <!DOCTYPE html>
        <html ${helmet.htmlAttributes.toString()}>
            <head>
                ${helmet.title.toString()}
                ${helmet.meta.toString()}
                ${helmet.link.toString()}
                ${helmet.script.toString()}
                ${helmet.style.toString()}
            </head>
            <body ${helmet.bodyAttributes.toString()}>
                <script>
                    window.__APP_CONFIG__ = Object.freeze(${JSON.stringify(config)});
                </script>
                <div id="app">\
`);

export class View extends Readable {
    config: App.Config;

    ctx: ParameterizedContext;

    constructor(config: App.Config, ctx: ParameterizedContext) {
        super();

        this.config = config;
        this.ctx = ctx;

        co.call(this, this.render).catch(ctx.onerror);
    }

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    _read(): void {}

    * render(): Iterator<unknown, void, undefined> {
        renderToString(<Head />);

        const helmet = Helmet.renderStatic();
        const routerContext: StaticRouterContext = {};

        const stream = renderToNodeStream(
            <StaticRouter location={this.ctx.req.url} context={routerContext}>
                <RecoilRoot>
                    <App />
                </RecoilRoot>
            </StaticRouter>,
        );

        stream.pipe(renderStylesToNodeStream());

        const body = yield (done: (err: unknown, res: unknown) => void): void => {
            stream.on('data', (data) => {
                setImmediate(() => done(null, data.toString()));
            });
        };

        this.push(start(this.config, helmet));

        this.push(body);

        this.push(end);

        this.push(null);
    }
}
