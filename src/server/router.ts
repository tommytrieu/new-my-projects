import Router from 'koa-router';

import { ssr } from 'src/server/middleware/ssr';

const router = new Router();

// change this to be more general later, with the previous value
// of /\.*/ there were issues where the ssr function was called
// multiple times
router.get(/\/\.*/, ssr);

export { router };
