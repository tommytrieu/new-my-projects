import { AbstractService } from 'src/services/AbstractService';

export class WorkEntityService extends AbstractService {
    /**
     * Gets all work entities for an owner
     */
    public async getAllWorks(): Promise<Services.WES.WorkEntity[]> {
        const url = '/api/v1/works';
        const headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${'AccessToken'}`, // get auth token here
            From: 'my-projects-page', // this could probably be shared among all services
        };

        try {
            const workEntities = await this.api.get(url, {
                headers,
                searchParams: {
                    tenant: 'VISTAPRINT-PROD', // eventually get this via better means
                },
            }) as unknown as Services.WES.WorkEntity[];

            return workEntities;
        } catch (error) {
            throw new Error();
        }
    }

    /**
     * Get work entity by workId
     * @param workId
     */
    public async getWorkEntity(workId: string): Promise<Services.WES.WorkEntity> {
        const url = `/api/v1/works/${workId}`;
        const headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${'AccessToken'}`, // get auth token here
            From: 'my-projects-page', // this could probably be shared among all services
        };

        try {
            const workEntity = await this.api.get(url, {
                headers,
                searchParams: {
                    tenant: 'VISTAPRINT-PROD', // eventually get this via better means
                },
            }) as unknown as Services.WES.WorkEntity;

            return workEntity;
        } catch (error) {
            throw new Error();
        }
    }
}
