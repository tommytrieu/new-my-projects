import ky, { Options } from 'ky';

export class AbstractService {
    api: typeof ky;

    config: Options;

    constructor(config: Options) {
        this.config = config;

        this.api = ky.create(config);
    }
}
