import { AppError } from 'src/shared/errors/AppError';

export class ServiceError extends AppError {
    constructor(payload: App.Error.Payload) {
        super(payload);

        this.name = 'ServiceError';
    }
}
