import { v4 as uuid } from 'uuid';

import * as utils from 'src/shared/errors/utils';

// TODO use a code for easy conditional handling of error
export class AppError extends Error {
    guid: string;

    status: number;

    constructor(payload: App.Error.Payload) {
        const message = utils.resolveMessage(payload);

        super(message);

        this.name = 'AppError';

        if ('stack' in payload) {
            this.stack = payload.stack;
        } else if (typeof Error.captureStackTrace === 'function') {
            Error.captureStackTrace(this, AppError);
        }

        this.guid = uuid();
        this.status = utils.resolveStatus(payload);

        Object.assign(this, payload);
    }
}
