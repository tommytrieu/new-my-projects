import { ServiceError } from 'src/shared/errors/ServiceError';

export type NotFoundErrorPayload = Error | {
    [key: string]: any;
    message: string;
    url: string;
};

export class NotFoundError extends ServiceError {
    constructor(payload: NotFoundErrorPayload, info?: Error) {
        super({ status: 404, ...payload, info });
        this.name = 'NotFoundError';
    }
}
