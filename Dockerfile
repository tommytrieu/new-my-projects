###############################################################################
# Location where our code is contained in the container
###############################################################################
ARG APP_DIR=/opt/app
###############################################################################

###############################################################################
# The build version. Used when creating an application version in EB 
###############################################################################
ARG APPLICATION_VERSION=
###############################################################################

###############################################################################
# The current git branch name. Used by Rollbar
###############################################################################
ARG BRANCH_NAME=
###############################################################################

###############################################################################
# DEBUG configuration
###############################################################################
ARG DEBUG=
###############################################################################

###############################################################################
# NODE_ENV configuration
###############################################################################
ARG NODE_ENV=production
###############################################################################

###############################################################################
# Token for VP npm artifactory
###############################################################################
ARG VP_ARTIFACTORY_TOKEN=
###############################################################################

###############################################################################
# Token for MCP npm artifactory
###############################################################################
ARG CIMPRESS_ARTIFACTORY_TOKEN=
###############################################################################

FROM node:15-alpine as base

ARG APP_DIR
ARG APPLICATION_VERSION
ARG BRANCH_NAME
ARG DEBUG
ARG NODE_ENV
ARG VP_ARTIFACTORY_TOKEN
ARG CIMPRESS_ARTIFACTORY_TOKEN

ENV APP_DIR=${APP_DIR}
ENV APPLICATION_VERSION=${APPLICATION_VERSION}
ENV BRANCH_NAME=${BRANCH_NAME}
ENV DEBUG=${DEBUG}
ENV NODE_ENV=${NODE_ENV}
ENV VP_ARTIFACTORY_TOKEN=${VP_ARTIFACTORY_TOKEN}
ENV CIMPRESS_ARTIFACTORY_TOKEN=${CIMPRESS_ARTIFACTORY_TOKEN}
ENV NEW_RELIC_DISTRIBUTED_TRACING_ENABLED=true

# App port
EXPOSE 80

WORKDIR ${APP_DIR}

# Suppress bluebird warnings in npm ci install
# ENV BLUEBIRD_WARNINGS 0

FROM base as build

COPY ./package-lock.json    ${APP_DIR}/package-lock.json
COPY ./package.json         ${APP_DIR}/package.json
COPY ./.npmrc               ${APP_DIR}/.npmrc

ONBUILD RUN apk add --update \
  g++ \
  make \
  python2 \
  python2-dev \
  && rm -rf /var/cache/apk/*

RUN echo "Building ${NODE_ENV}..."

FROM build as development

ONBUILD RUN npm install
ONBUILD COPY . ${APP_DIR}/

CMD ["npm", "run", "start:dev"]

FROM build as production

ONBUILD RUN VP_ARTIFACTORY_TOKEN=${VP_ARTIFACTORY_TOKEN}  CIMPRESS_ARTIFACTORY_TOKEN=${CIMPRESS_ARTIFACTORY_TOKEN} npm ci

# ONBUILD COPY ./newrelic.js  ${APP_DIR}/newrelic.js
# ONBUILD COPY ./config       ${APP_DIR}/config
ONBUILD COPY ./dist         ${APP_DIR}/dist

ONBUILD RUN chmod +x ./dist/server.js

CMD ["npm", "start"]

FROM ${NODE_ENV}
