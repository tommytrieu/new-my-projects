const path = require('path');

const IS_DEVELOPMENT = process.env.NODE_ENV === 'development';
const PUBLIC_PORT = process.env.PUBLIC_PORT || 8080;
const PUBLIC_PATH = `http://localhost:${PUBLIC_PORT}/static/`;
const SERVER_OUTPUT_PATH = path.resolve(process.cwd(), 'dist');
const STATIC_PATH = path.resolve(process.cwd(), 'public');

module.exports = {
    IS_DEVELOPMENT,
    PUBLIC_PATH,
    STATIC_PATH,
    SERVER_OUTPUT_PATH,
};
