const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

const constants = require('./constants');

module.exports = {
    target: 'web',
    entry: './src/client/index.tsx',
    mode: constants.IS_DEVELOPMENT ? 'development' : 'production',
    devtool: constants.IS_DEVELOPMENT ? 'eval-source-map' : 'source-map',
    devServer: {
        compress: true,
        contentBase: constants.STATIC_PATH,
        contentBasePublicPath: constants.PUBLIC_PATH,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        host: '0.0.0.0',
        hot: true,
        port: constants.PUBLIC_PORT,
        publicPath: constants.PUBLIC_PATH,
        sockPort: constants.PUBLIC_PORT,
    },
    output: {
        publicPath: constants.PUBLIC_PATH,
        filename: 'index.js',
        path: constants.STATIC_PATH,
    },
    plugins: [
        new CleanWebpackPlugin(),
    ],
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
        plugins: [new TsconfigPathsPlugin()],
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'ts-loader',
                    },
                ],
            },
        ],
    },
};
