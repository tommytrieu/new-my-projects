/* eslint-disable import/no-extraneous-dependencies */
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const NodemonPlugin = require('nodemon-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const webpack = require('webpack');

const constants = require('./constants');

module.exports = {
    entry: './src/server/index.ts',
    target: 'node',
    mode: constants.IS_DEVELOPMENT ? 'development' : 'production',
    devtool: constants.IS_DEVELOPMENT ? 'eval-source-map' : 'source-map',
    externals: [
        {
            formidable: 'commonjs formidable',
        },
    ],
    output: {
        filename: 'index.js',
        path: constants.SERVER_OUTPUT_PATH,
        publicPath: constants.PUBLIC_PATH,
    },
    plugins: [
        new NodemonPlugin({
            ext: 'js,ts,tsx,json,yml',
            ignore: [
                '.git',
                'dist',
                'node_modules',
            ],
            nodeArgs: ['--max-old-space-size=8192', '--inspect=0.0.0.0:9229'],
            verbose: true,
        }),
        new CleanWebpackPlugin(),
        new webpack.DefinePlugin({
            PUBLIC_PATH: JSON.stringify(constants.PUBLIC_PATH),
        }),
        new webpack.WatchIgnorePlugin({
            paths: [
                'node_modules',
                '__tests__',
                '__mocks__',
            ],
        }),
        constants.IS_DEVELOPMENT ? null : new webpack.ids.HashedModuleIdsPlugin(),
    ].filter(Boolean),
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
        plugins: [new TsconfigPathsPlugin()],
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'ts-loader',
                    },
                ],
            },
        ],
    },
};
