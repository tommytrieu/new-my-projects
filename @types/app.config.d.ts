declare namespace App {
    export type Config = {
        appName: string;
        auth: {
            clientId: string;
            developmentMode: boolean;
        };
    };
}
