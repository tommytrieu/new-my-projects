declare namespace Services.SiteFlow {
    export interface SiteFlowResponse extends Flow {
        predictedFlow: Flow[]
    }

    export interface Flow {
        action: number;
        url: string;
    }
}
