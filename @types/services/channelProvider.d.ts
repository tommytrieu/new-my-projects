declare namespace Services.ChannelProvider {
    export interface ContactUsPageResponse {
        url: string;
    }
}
