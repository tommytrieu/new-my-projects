declare namespace Services.AILS {
    export interface albelliDoc {
        attributes: Record<string, string>,
        canonicalId: string,
        createdAt: number,
        deleted: true,
        docRefStatus: string,
        docRefUrl: string,
        documentId: string,
        images: Image[],
        lastUpdatedAt: number,
        name: string,
        pdfStatus: string,
        pdfUrl: string,
        previewImage: Image[],
        previewMcpImageId: string,
        productCode: string,
        projectType: string,
        vpEditUrl: string
    }

    export interface Image {
        height: number,
        imageId: string,
        imageUrl: string,
        width: number
    }
}
