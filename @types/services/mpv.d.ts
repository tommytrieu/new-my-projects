declare enum GetStartedLink { ProductPage = 'ProductPage', TLP = 'TLP' }

declare namespace Services.MPV.Models {
    export interface MerchandisingProductViewV2 {
        mpvId: string;
        name: string;
        urlSegment: string;
        coreProductId: string;
        quantities: Quantities;
        externallyMarketable: boolean;
        uploadOnly: boolean;
        temporarilyOutOfStock: boolean;
        merchandisingDetails: MerchandisingDetailsV2[];
        defaultProductImage?: Image;
        defaultPreselectedOptions: MpvOptionV2[];
        desiredGetStartedLink: GetStartedLink;
    }

    export interface MpvOptionV2 {
        key: string;
        name?: string;
        marketingText?: string;
        isPopular: boolean;
        isPremium: boolean;
        isRecommended: boolean;
        image: Image[];
        icon?: import('@vp/cloudinary-image').IImage;
        coreModelMappings: Record<string, string>;
    }

    export interface PropertyV2 {
        key: string;
        name?: string;
        callToAction?: string;
        marketingText?: string;
        options: MpvOptionV2[];
    }

    export interface CountryOverride {
        country: string;
        default: number;
    }

    export interface MerchandisingDetailsV2 {
        type: string;
        preSelectedOptions: MpvOptionV2[];
        properties: PropertyV2[];
    }

    export interface Quantities {
        default?: number;
        recommended?: number;
        quantities?: number[];
        merchDetailsOverrides?: any;
        quantityOverrides?: Record<string, CountryOverride>;
    }

    export interface Image {
        cloudinaryImage: import('@vp/cloudinary-image').IImage;
        altTextOverride?: string;
        seoUrlSuffix?: string;
    }
}
