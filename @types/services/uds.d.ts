declare namespace Services.UDS {
    export interface CimDoc {
        version: string;
        document: Document;
        sku?: string;
        fontRepositoryUrl: string;
        metadata?: Record<string, string>;
        projectionId?: string;
        documentId?: string;
        deleted?: boolean;
        revision?: number;
        owner?: string;
        tenant?: string;
    }

    export interface ImageOverlay {
        color: string;
        printUrl: string;
        previewUrl: string;
    }

    export interface ColorOverrides {
        ordinal: number;
        color: string;
    }

    export interface CropFractions {
        bottom: string;
        top: string;
        left: string;
        right: string;
    }

    export interface ImageItem {
        id: string;
        zIndex: number;
        position: import('@design-stack/utility/measurement/types').MeasurementBoundingRectangle;
        rotationAngle?: string;
        horizontalAlignment?: string;
        verticalAlignment?: string;
        printUrl: string;
        previewUrl: string;
        originalSourceUrl?: string;
        pageNumber: number;
        overlays?: ImageOverlay[];
        cropFractions?: CropFractions;
        colorOverrides?: ColorOverrides[];
        vpThreadMapping?: string;
    }

    export interface TextField {
        fontSize: string;
        fontStyle: string;
        fontFamily: string;
        content: string;
        color: string;
        overprints?: string[];
        inlineBaseDirection?: string;
        id?: string;
    }

    export interface TextAreaItem {
        id: string;
        zIndex: number;
        position: import('@design-stack/utility/measurement/types').MeasurementBoundingRectangle;
        rotationAngle?: string;
        horizontalAlignment?: string;
        verticalAlignment?: string;
        blockFlowDirection?: string;
        textOrientation?: string;
        textFields: TextField[];
    }

    export interface ItemReference {
        id: string;
        type?: string;
        url: string;
        specName?: string;
        data: Record<string, string>;
        zIndex: number;
        rotationAngle?: string;
        position: import('@design-stack/utility/measurement/types').MeasurementBoundingRectangle;
    }

    export interface Stroke {
        color: string;
        thickness: string;
    }

    export interface StrokeLineAttributes {
        lineCap: string;
        lineJoin: string;
    }

    export interface LineItem {
        readonly type: 'line';
        id: string;
        zIndex: number;
        rotationAngle?: string;
        start: import('@design-stack/utility/measurement/types').MeasurementCoordinates;
        end: import('@design-stack/utility/measurement/types').MeasurementCoordinates;
        stroke: Stroke & StrokeLineAttributes;
    }

    export interface FigureItem {
        readonly type: 'rectangle' | 'ellipse';
        id: string;
        position: import('@design-stack/utility/measurement/types').MeasurementBoundingRectangle;
        zIndex: number;
        rotationAngle?: string;
        stroke: Stroke;
        color: string;
    }

    export interface ComparisonImage {
        key: string;
        url: string;
    }

    export interface SimpleTextField extends TextItem {
        content: string;
        rotationAngle: string;
        fontSize: string;
        fontStyle: string;
        fontFamily: string;
        color: string;
        overprints: string[];
        stroke: {
            color: string;
            overprints: string[];
            thickness: string;
            lineCap: string;
            lineJoin: string;
        };
    }

    export interface Ornament {
        type: string;
        positions: import('@design-stack/utility/measurement/types').MeasurementCoordinates;
    }

    export interface Panel {
        id: string;
        name: string;
        width: string;
        height: string;
        decorationTechnology: string;
        images?: ImageItem[];
        textAreas?: TextAreaItem[];
        itemReferences?: ItemReference[];
        shapes?: ShapeItem[];
        comparisonImages?: ComparisonImage[];
        simpleTextFields?: SimpleTextField[];
        ornaments?: Ornament[];
    }

    export interface Document {
        panels: Panel[];
    }

    export type TextItem = Omit<TextAreaItem, 'textFields'>;
    export type ShapeItem = LineItem | FigureItem;
    export type Item = ImageItem | TextAreaItem | ShapeItem | ItemReference;
}
