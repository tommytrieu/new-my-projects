declare namespace Services.WES {
    export interface WorkEntity {
        created: string;
        design: Design;
        modified: string;
        merchandising: Merchandising;
        ownerId: string;
        resources?: Record<string, string>;
        workId: string;
        workName?: string;
        workRevisionId: string;
    }

    export interface Design {
        designUrl: string;
        displayUrl: string;
        editUrl: string;
        manufactureUrl: string;
        metadata?: Record<string, string>;
    }

    export interface Merchandising {
        merchandisingSelections: Record<string, string>;
        mpvUrl: string;
        quantity?: number;
    }
}
