declare namespace App.Error {
    export type Payload = import('ky').default.HTTPError | Error | {
        [key: string]: any | null | undefined;
        message: string;
        status?: number;
        url?: string;
    };
}
